package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Optional;

public final class DockedShip implements ModularVessel {

  private String name;

  private PositiveInteger shieldHP;

  private PositiveInteger hullHP;

  private PositiveInteger capacitor;

  private PositiveInteger capacitorRegeneration;

  private PositiveInteger pg;

  private AttackSubsystem attackSubsystem;

  private DefenciveSubsystem defenciveSubsystem;

  public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
                                     PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate) {
    DockedShip dockedShip = new DockedShip();
    dockedShip.name = name;
    dockedShip.shieldHP = shieldHP;
    dockedShip.hullHP = hullHP;
    dockedShip.capacitor = capacitorAmount;
    dockedShip.capacitorRegeneration = capacitorRechargeRate;
    dockedShip.pg = powergridOutput;
    return dockedShip;
  }


  @Override
  public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
    Optional<AttackSubsystem> opt = Optional.ofNullable(subsystem);
    if (opt.isPresent()) {
      decreasePg(subsystem.getPowerGridConsumption().value());
      this.attackSubsystem = subsystem;
    } else {
      this.attackSubsystem = null;
    }
  }

  @Override
  public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
    Optional<DefenciveSubsystem> opt = Optional.ofNullable(subsystem);
    if (opt.isPresent()) {
      decreasePg(subsystem.getPowerGridConsumption().value());
      this.defenciveSubsystem = subsystem;
    } else {
      this.defenciveSubsystem = null;
    }

  }

  private void decreasePg(Integer consumption) throws InsufficientPowergridException {
    int diff = pg.value() - consumption;
    if (diff >= 0) {
      pg = new PositiveInteger(diff);
    } else {
      throw new InsufficientPowergridException(Math.abs(diff));
    }
  }

  public CombatReadyShip undock() throws NotAllSubsystemsFitted {
    if (attackSubsystem == null && defenciveSubsystem == null) {
      throw NotAllSubsystemsFitted.bothMissing();
    }
    if (attackSubsystem == null) {
      throw NotAllSubsystemsFitted.attackMissing();
    }
    if (defenciveSubsystem == null) {
      throw NotAllSubsystemsFitted.defenciveMissing();
    }
    return new CombatReadyShip(this);
  }

  public String getName() {
    return name;
  }

  public PositiveInteger getShieldHP() {
    return shieldHP;
  }

  public PositiveInteger getHullHP() {
    return hullHP;
  }

  public PositiveInteger getCapacitor() {
    return capacitor;
  }

  public PositiveInteger getCapacitorRegeneration() {
    return capacitorRegeneration;
  }

  public PositiveInteger getPg() {
    return pg;
  }

  public AttackSubsystem getAttackSubsystem() {
    return attackSubsystem;
  }

  public DefenciveSubsystem getDefenciveSubsystem() {
    return defenciveSubsystem;
  }

  public void setCapacitor(PositiveInteger capacitor) {
    this.capacitor = capacitor;
  }
}
