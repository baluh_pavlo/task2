package com.binary_studio.fleet_commander.core.ship;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.NamedEntity;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;

import java.util.Optional;

public final class CombatReadyShip implements CombatReadyVessel {

  private DockedShip dockedShip;

  private PositiveInteger baseCap;

  private Integer attacksToDestroy;

  public CombatReadyShip(DockedShip dockedShip) {
    this.baseCap = dockedShip.getCapacitor();
    attacksToDestroy = 5;
    this.dockedShip = dockedShip;
  }

  @Override
  public void endTurn() {
    Integer newCap = dockedShip.getCapacitor().value() + dockedShip.getCapacitorRegeneration().value();
    if (newCap >= baseCap.value()) {
      dockedShip.setCapacitor(new PositiveInteger(baseCap.value()));
    } else {
      dockedShip.setCapacitor(new PositiveInteger(newCap));
    }
  }

  @Override
  public void startTurn() {
  }

  @Override
  public String getName() {
    return dockedShip.getName();
  }

  @Override
  public PositiveInteger getSize() {
    return null;
  }

  @Override
  public PositiveInteger getCurrentSpeed() {
    return null;
  }

  @Override
  public Optional<AttackAction> attack(Attackable target) {
    if (dockedShip.getAttackSubsystem().getCapacitorConsumption().value() > dockedShip.getCapacitor().value()) {
      return Optional.empty();
    } else {
      dockedShip.setCapacitor(new PositiveInteger(dockedShip.getCapacitor()
              .value() - dockedShip.getAttackSubsystem().getCapacitorConsumption().value()));
      PositiveInteger damage = dockedShip.getAttackSubsystem().attack(target);
      NamedEntity targtName = target::getName;
      NamedEntity attacker = dockedShip::getName;
      NamedEntity weapon = dockedShip.getAttackSubsystem()::getName;
      AttackAction attackAction = new AttackAction(damage, attacker, targtName, weapon);
      return Optional.of(attackAction);
    }
  }

  @Override
  public AttackResult applyAttack(AttackAction attack) {
    if (attacksToDestroy > 0) {
      attacksToDestroy--;
      AttackAction newAttack = dockedShip.getDefenciveSubsystem().reduceDamage(attack);
      return new AttackResult.
              DamageRecived(attack.weapon, newAttack.damage, attack.target);
    }
    return new AttackResult.Destroyed();
  }

  @Override
  public Optional<RegenerateAction> regenerate() {
    if (dockedShip.getDefenciveSubsystem().getCapacitorConsumption().value() > dockedShip.getCapacitor().value()) {
      return Optional.empty();
    } else {
      dockedShip.setCapacitor(PositiveInteger.of(dockedShip.getCapacitor().value() - dockedShip.getDefenciveSubsystem()
              .getCapacitorConsumption().value()));
    }
    return Optional.of(dockedShip.getDefenciveSubsystem().regenerate());

  }

}
