package com.binary_studio.fleet_commander.core.common;

import java.util.Objects;

public final class PositiveInteger {

  private final Integer underlyingVal;

  public PositiveInteger(Integer val) {
    if (val < 0) {
      throw new IllegalArgumentException(String.format("Got negative value %d, expected positive integer", val));
    }
    this.underlyingVal = val;
  }

  public static PositiveInteger of(Integer val) {
    return new PositiveInteger(val);
  }

  public static PositiveInteger of(Double val) {
    return new PositiveInteger(val.intValue());
  }

  public Integer value() {
    return this.underlyingVal;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;
    PositiveInteger that = (PositiveInteger) o;
    return Objects.equals(underlyingVal, that.underlyingVal);
  }

  @Override
  public int hashCode() {
    return this.underlyingVal.hashCode();
  }


}
