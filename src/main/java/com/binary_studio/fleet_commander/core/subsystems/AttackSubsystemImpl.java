package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;

import java.util.Optional;

public final class AttackSubsystemImpl implements AttackSubsystem {

  private String name;

  private PositiveInteger baseDamage;

  private PositiveInteger optimalSize;

  private PositiveInteger optimalSpeed;

  private PositiveInteger capacitorUsage;

  private PositiveInteger pgRequirement;

  public static AttackSubsystemImpl construct(String name, PositiveInteger powergridRequirments,
                                              PositiveInteger capacitorConsumption,
                                              PositiveInteger optimalSpeed,
                                              PositiveInteger optimalSize,
                                              PositiveInteger baseDamage) throws IllegalArgumentException {
    AttackSubsystemImpl attackSubsystem = new AttackSubsystemImpl();
    attackSubsystem.name = Optional.ofNullable(name).filter(s -> !s.trim().isEmpty())
            .orElseThrow(() -> new IllegalArgumentException("Name should be not null and not empty"));
    attackSubsystem.baseDamage = baseDamage;
    attackSubsystem.optimalSize = optimalSize;
    attackSubsystem.optimalSpeed = optimalSpeed;
    attackSubsystem.capacitorUsage = capacitorConsumption;
    attackSubsystem.pgRequirement = powergridRequirments;
    return attackSubsystem;
  }


  @Override
  public PositiveInteger getPowerGridConsumption() {
    return pgRequirement;
  }

  @Override
  public PositiveInteger getCapacitorConsumption() {
    return capacitorUsage;
  }

  @Override
  public PositiveInteger attack(Attackable target) {
    double sizeReductionModifier = target.getSize().value() >= optimalSize.value() ? 1
            : target.getSize().value().floatValue() / optimalSize.value().floatValue();
    double speedReductionModifier = target.getCurrentSpeed().value() <= optimalSpeed.value() ? 1
            : optimalSpeed.value().floatValue() / (2 * target.getCurrentSpeed().value().floatValue());
    PositiveInteger damage = PositiveInteger.of(Math.ceil(baseDamage.value()
            * Math.min(sizeReductionModifier, speedReductionModifier)));

    return damage.value()>0?damage:PositiveInteger.of(1);
  }


  @Override
  public String getName() {
    return name;
  }

}
