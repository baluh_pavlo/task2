package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import java.util.Optional;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

  private String name;

  private PositiveInteger impactReduction;

  private PositiveInteger shieldRegen;

  private PositiveInteger hullRegen;

  private PositiveInteger capacitorUsage;

  private PositiveInteger pgRequirement;

  public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
                                                 PositiveInteger capacitorConsumption,
                                                 PositiveInteger impactReductionPercent,
                                                 PositiveInteger shieldRegeneration,
                                                 PositiveInteger hullRegeneration) throws IllegalArgumentException {
    DefenciveSubsystemImpl defenciveSubsystem = new DefenciveSubsystemImpl();
    defenciveSubsystem.name = Optional.ofNullable(name).filter(s -> !s.trim().isEmpty())
            .orElseThrow(() -> new IllegalArgumentException("Name should be not null and not empty"));
    defenciveSubsystem.impactReduction =
            impactReductionPercent.value() <= 95 ? impactReductionPercent : new PositiveInteger(95);
    defenciveSubsystem.shieldRegen = shieldRegeneration;
    defenciveSubsystem.hullRegen = hullRegeneration;
    defenciveSubsystem.capacitorUsage = capacitorConsumption;
    defenciveSubsystem.pgRequirement = powergridConsumption;
    return defenciveSubsystem;
  }


  @Override
  public PositiveInteger getPowerGridConsumption() {
    return pgRequirement;
  }

  @Override
  public PositiveInteger getCapacitorConsumption() {
    return capacitorUsage;
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  public AttackAction reduceDamage(AttackAction incomingDamage) {
    incomingDamage.damage = PositiveInteger
            .of(Math.ceil(incomingDamage.damage.value() * (1 - impactReduction.value().floatValue() / 100)));
    return incomingDamage;
  }

  @Override
  public RegenerateAction regenerate() {
    return new RegenerateAction(shieldRegen, hullRegen);
  }

}
