package com.binary_studio.uniq_in_sorted_stream;

import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

public final class UniqueSortedStream {

  private static Set<Long> keySet = ConcurrentHashMap.newKeySet();


  private UniqueSortedStream() {
  }

  public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
    Stream<Row<T>> rowStream = stream.filter((e) -> keySet.add(e.getPrimaryId()));
    keySet.clear();
    return rowStream;
  }

}
