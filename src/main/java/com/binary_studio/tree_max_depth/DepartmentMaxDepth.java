package com.binary_studio.tree_max_depth;

import java.util.ArrayDeque;
import java.util.Queue;

public final class DepartmentMaxDepth {

  private DepartmentMaxDepth() {
  }



  /**
   * This method is used to calculate height of  tree represented by array.
  */
  public static Integer calculateMaxDepth(Department rootDepartment) {
    Integer height = 0;
    if (rootDepartment == null) {
      return 0;
    }
    if (rootDepartment.subDepartments.isEmpty()) {
      return 1;
    }
    Queue<Department> queue = new ArrayDeque<>();
    queue.add(rootDepartment);
    while (!queue.isEmpty()) {
      int numberOfNodes = queue.size();
      while (numberOfNodes-- > 0) {
        Department front = queue.poll();
        Department leftChild = front.subDepartments.size() > 0 ? front.subDepartments.get(0) : null;
        Department rightChild = front.subDepartments.size() > 1 ? front.subDepartments.get(1) : null;
        if (leftChild != null) {
          queue.add(leftChild);
        }
        if (rightChild != null) {
          queue.add(rightChild);
        }
      }
      height++;
    }

    return height;
  }

}
